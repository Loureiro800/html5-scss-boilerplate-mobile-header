// Responsive Navigation
$("body.show_sidebar #wrapper").click(function() {
	$("body").removeClass("show_sidebar");
});
$(".menu-btn, .navbar-mobile, body.show_sidebar #wrapper, .menu-close, .menu-close-cover").click(function() {

	$("body").toggleClass("show_sidebar");
	$(".navbar-mobile .fa").toggleClass("fa-navicon fa-times"); // toggle 2 classes in Jquery: http://goo.gl/3uQAFJ - http://goo.gl/t6BQ9Q
	$(".menu-toggle").fadeToggle(300);
});

// Smooth Scrolling To Internal Links
$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault(event);

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
});
